const mongoos = require('mongoose');
require('dotenv').config();


const connectDB = () => {
    mongoos.connect(process.env.CONNECTION_URI, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        }).then(db => console.log("Database connected."))
        .catch(err => console.log(err));
};

connectDB();