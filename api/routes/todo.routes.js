const express = require('express');
const router = express.Router();
const { addTodo, updateTodo, updateStatusTodo, getTodo, getTodos, deleteTodo } = require('../controllers/todo.controller');
const { verifyToken } = require('../controllers/verify.controller');

router.get('/', verifyToken, getTodos);
router.get('/:id', verifyToken, getTodo);
router.post('/', verifyToken, addTodo);
router.put('/:id', verifyToken, updateTodo);
router.put('/status/:id', verifyToken, updateStatusTodo);
router.delete('/:id', verifyToken, deleteTodo);


module.exports = router;