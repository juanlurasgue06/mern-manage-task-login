const { Schema, model } = require('mongoose');

const todoSchema = new Schema({
    title: String,
    description: String,
    owner: String,
    todoStatus: {
        type: Boolean,
        default: false
    },
    complete_at: {
        type: Date,
        default: null
    },
    create_at: {
        type: Date,
        default: new Date()
    }
});

todoSchema.methods.isCompleted = function() {
    return !this.todoStatus;
};

module.exports = model('todo', todoSchema);