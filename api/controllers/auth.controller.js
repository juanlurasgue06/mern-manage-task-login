const User = require("../models/user");
const jwt = require("jsonwebtoken");

const authMethods = {};

authMethods.signup = async(req, res) => {
    const { username, password } = req.body;
    const newUser = new User({ username, password });

    newUser.password = await newUser.encryptPassword(password);

    await newUser.save();

    res.json({
        status: 200,
        message: 'User Saved'
    });
};

authMethods.signin = async(req, res) => {
    const { username, password } = req.body;
    const user = await User.findOne({ username: username });

    if (!user) {
        res.json({
            status: 500,
            message: 'Username or password incorrect.'
        });
    }
    const autenticate = await user.confirmPassword(password);

    if (!autenticate) {
        res.json({
            status: 500,
            message: 'Username or password incorrect.'
        });
    }

    const token = jwt.sign({ uid: user._id.toString() }, process.env.SECURE_KEY, {
        expiresIn: 60 * 60 * 5
    });

    if (!token) {
        res.json({
            status: 500,
            message: 'There was a problem, try it again.'
        });
    }

    res.json({
        status: 200,
        token
    });

};

module.exports = authMethods;