const todo = require("../models/todo");

const todoMethods = {};


todoMethods.getTodo = async(req, res) => {
    const todos = await todo.findById(req.params.id);

    res.json({
        status: 200,
        todos
    });
};

todoMethods.getTodos = async(req, res) => {
    const { uid } = req.userID;
    const todos = await todo.find({ "owner": uid });

    res.json({
        status: 200,
        todos
    });
};

todoMethods.addTodo = async(req, res) => {
    const { title, description } = req.body;
    const { uid } = req.userID;
    const newTodo = new todo({ title, description, "owner": uid });

    newTodo.save();

    res.json({
        status: 200,
        message: 'Todo Saved'
    });

};

todoMethods.updateTodo = async(req, res) => {
    const { title, description } = req.body;
    console.log(req.params.id);
    await todo.findByIdAndUpdate(req.params.id, { title, description });

    res.json({
        status: 200,
        message: 'Todo Updated'
    });
};

todoMethods.updateStatusTodo = async(req, res) => {
    const todoU = await todo.findById(req.params.id);
    const newStatus = todoU.isCompleted();

    if (newStatus)
        await todo.findByIdAndUpdate(req.params.id, { todoStatus: newStatus, complete_at: new Date() });
    else
        await todo.findByIdAndUpdate(req.params.id, { todoStatus: newStatus, complete_at: null });

    res.json({
        status: 200,
        message: 'Todo Status Updated'
    });

};

todoMethods.deleteTodo = async(req, res) => {
    await todo.findByIdAndDelete(req.params.id);

    res.json({
        status: 200,
        message: 'Todo Deleted'
    });
};

module.exports = todoMethods;