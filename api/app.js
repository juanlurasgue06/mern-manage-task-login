const express = require('express');
const helmet = require('helmet');
const cors = require('cors');
const morgan = require('morgan');
const app = express();

// Body parser
app.use(express.json());

// Other config
app.use(cors());
app.use(helmet());
app.use(morgan('dev'));

//Routes
app.use(require('./routes/auth.todo'));
app.use('/todo', require('./routes/todo.routes'));

module.exports = app;