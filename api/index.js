const app = require('./app');
require('dotenv');
require('./database');

const init = async() => {
    await app.listen(process.env.PORT || 3001);
    console.log("Server started at " + process.env.PORT || 3001);
};

init();